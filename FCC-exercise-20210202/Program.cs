﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FCC_exercise_20210202
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("FCC Dice Game Simulation - Jonathan Hack");

            double iterations = 10000;
            var numberOfDice = 5;
            var diceSides = 6;
            var simulationStart = DateTime.UtcNow;
            Random _rng = new Random();

            Console.WriteLine($"Number of simulations was {iterations} using {numberOfDice} {((numberOfDice > 1) ? "dice" : "die")}.");

            List<DiceGame> results = new List<DiceGame>();

            for (var i = 0; i < iterations; i++)
            {
                var gameIteration = new DiceGame(_rng, numberOfDice, diceSides);
                gameIteration.RunGame();
                results.Add(gameIteration);
            }

            var simulationEnd = DateTime.UtcNow;

            var groupedResults = results.OrderBy(x => x.score).GroupBy(x => x.score);

            foreach (var group in groupedResults)
            {
                var grpScore = group.First().score;
                var scorePercentage = Math.Round(group.Count() / iterations, 2);

                Console.WriteLine($"Total {grpScore} occurs {scorePercentage} occurred {group.Count()} times.");
            }

            var executionTime = (simulationEnd - simulationStart).TotalMilliseconds;
            Console.WriteLine($"Total simulation took {executionTime} milliseconds.");
            Console.ReadKey();
        }
    }
}
