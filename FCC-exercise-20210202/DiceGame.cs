﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FCC_exercise_20210202
{
    class DiceGame
    {
        private int _numberOfDice;
        private int _diceSides;
        private double _score { get; set; }
        private Random _rng;

        public double score
        {
            get { return _score; }            
        }

        public DiceGame(Random rng, int numberOfDice = 2, int diceSides = 6)
        {
            _numberOfDice = numberOfDice;
            _diceSides = diceSides;
            _rng = rng;
        }

        public void RunGame()
        {
            var diceLeft = _numberOfDice;

            while (diceLeft > 0)
            {
                List<int> roll = new List<int>();
                for (var i = diceLeft; i > 0; i--)
                {
                    roll.Add(_rng.Next(1, _diceSides));
                }

                var threesRolled = roll.Where(x => x == 3);

                if (threesRolled.Count() > 0)
                {
                    diceLeft -= threesRolled.Count();
                }
                else
                {
                    var lowestRoll = roll.Min();
                    _score += lowestRoll;
                    diceLeft--;
                }
            }
        }
    }
}
